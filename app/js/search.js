"use strict";
app.controller('SearchController', function SearchController($scope, $location, $cookies, params, res) {
    $scope.params = set_defaults(params, { skip: 0, q: '' });

    Object.defineProperty($scope, 'take', {
        get: function() {
            var take = $cookies.get('take');
            return parseFloat(take) || 10;
        },
        set: function(value) {
            if (value && isFinite(value)) {
                $cookies.put('take', value, {
                    expires: new Date(Date.now() + 365 * 24 * 3600 * 1000).toGMTString()
                });
                $scope.query(0);
            }
        }
    });

    Object.defineProperty($scope, 'page', {
        get: function() { return Math.floor((this.params.skip || 0) / this.take + 1); },
        set: function(value) {
            params.skip = (value - 1) * this.take;
            go();
        }
    });

    var firms = new res('/search');

    $scope.query = function(skip) {
        if ('number' == typeof skip)
            params.skip = skip;
        if (0 == params.skip)
            delete params.skip;
        $scope.firms = firms.query(params, function() {
            if (params.q.length > 0 && !params.skip)
                $scope.count = firms.get({count: params.q});
        }, error_dump);
    };
    $scope.query();

    function go() {
        if (params.skip <= 0)
            delete params.skip;
        $location.search(params);
    }
});
