"use strict";
var qs = require('querystring');

module.exports = {
    cat: function (req, res) {
        var id = req.query.id;
        var q = ['select * from cat where'];
        var params = [];
        if (id) {
            q.push('parent = $ or id = $');
            params.push(id);
            params.push(id);
        }
        else
            q.push('level = 0');
        q.push('order by level desc, name asc');
        query(q, params, send_rows(res));
    },

    search: function(req, res) {
        var search = qs.unescape(req.query.q);
        log.debug('/cat/search ' + search);
        search = search.replace('+', '%');
        db.query('select "id", "name" from "cat" where "name" ilike $1 offset 0 limit 10',
            ['%' + search + '%'], send_rows(res));
    }
};
