drop view firm_cat;
create view firm_cat as
  select firm_id as firm, rubricator_bit as cat from prices
  group by firm_id, rubricator_bit;

drop view firm_good;
create view firm_good as
  select firm_id as firm, goods_id as good from prices
  group by firm_id, goods_id;
