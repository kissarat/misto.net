"use strict";
var Cookies = require('cookies');

module.exports = {
    firm: function firm(req, res) {
        var id = req.query.id;
        switch (req.method) {
            case 'GET':
                db.query('select * from firm where id=$1', [id],
                    wrap(function(result) {
                        if (0 == result.rowCount)
                            return res.sendStatus(404);
                        result = result.rows[0];
                        db.query('select id, name from cat join firm_cat on id = cat where firm = $1', [id],
                            wrap(function(cat_result) {
                                result.cats = cat_result.rows;
                                res.json(result);
                            }));
                    }));
                break;

            case 'POST':
                update(res, 'firm', {id:id}, req.body);
                break;
        }

    },

    search: function(req, res) {
        var cookies = new Cookies(req, res);
        if ('count' in req.query) {
            db.query('select count(*) as "count" from firm where name ilike $1', ['%' + req.query.count + '%'],
                wrap(function(result) {
                    res.json({count:+result.rows[0].count});
                }));
            return;
        }
        var skip = req.query.skip;
        if (skip) {
            skip = parseInt(skip);
            if (skip < 0)
                skip = 0;
        }
        else
            skip = 0;
        var take = cookies.get('take');
        if (take) {
            take = parseInt(take);
            if (take > 100)
                take = 100;
            else if (take < 0)
                take = 20;
        }
        else
            take = 20;
        var search = req.query.q;
        var cat = req.query.cat;
        var params = [];
        var where = [];
        var q = ['select id, name, address, phone from firm'];
        if (cat)
            q.push('join firm_cat on id = firm');
        if (search) {
            where.push('name ilike $');
            params.push('%' + search + '%');
        }
        if (cat) {
            where.push('cat = $');
            params.push(cat);
        }
        if (where.length > 0)
            q.push('where ' + where.join(' and '));
        q.push('order by rank desc');
        q.push('offset $');
        params.push(skip);
        q.push('limit $');
        params.push(take);
        query(q, params, send_rows(res));
    }
};
