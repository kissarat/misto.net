"use strict";
module.exports = function note(req, res) {
    function query() {
        db.query('select * from "sorted_note" where "parent"=$1 or "id"=$1', [req.param.id || 0], send_rows(res));
    }

    if ('POST' == req.method)
        auth(req, res, function(user) {
            db.query('insert into "note"("author", "text", "topic", "parent", "ip") values ($1,$2,$3,$4,$5)',
                [user.username,
                    req.body.text, req.body.topic, req.body.parent, req.body.ip], wrap(query));
        });
    else
        query();
};
