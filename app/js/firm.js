"use strict";
app.controller('FirmController', function FirmController($scope, $cookies, $location, $window, params, res) {
    var firm = new res('/firm');
    var editMode = 0 == location.pathname.indexOf('/firm/edit');
    $scope.edit = 'admin' == $cookies.get('username') ? function() {
        $location.path('/firm/edit').search(params);
    } : false;

    if (editMode) {
        $scope.save = function() {
            firm.save($scope.firm)
        };

        $scope.cancel = function() {
            $window.history.back();
        };

        var cats = {};
        $scope.new_catalog = '';
        var autocomplete = new res('/cat/search');
        var delay = true;
        $scope.cat_change = function() {
            if (delay) {
                delay = false;
                setTimeout(function() {
                    var value = $scope.new_catalog;
                    $scope.autocomplete = autocomplete.query({q:value.trim()}, function(data) {
                        for(var i = 0; i < data.length; i++)
                            cats[data[i].name.trim()] = data[i].id;
                    });
                    delay = true;
                }, 300);
            }
        };

        $scope.select_cat = function(e) {
            if (13 == e.keyCode) {
                console.log(cats[e.target.value.trim()]);
            }
        }
    }

    $scope.firm = firm.get(params, function(data) {
        if (editMode)
            return;
        if (!data.street)
            return;
        if (data.house)
            data.search = data.house + ', вулиця ' + data.street;
        else
            data.search = data.street;
        $scope.google_maps = data.city + ', ' + data.search;
        $scope.google_maps = encodeURIComponent($scope.google_maps);
        //$scope.google_maps = $scope.google_maps.replace('%20', '+');
        var nominatim = new res('http://nominatim.openstreetmap.org/search.php', {
            format: 'json',
            'accept-language': 'uk',
            country: 'Україна'
        });
        nominatim.query({
            city: data.city,
            street: data.search
        }, function (geo) {
            if (geo.length > 0) {
                geo = geo[0];
                console.log(geo.display_name);
                var marker = new google.maps.LatLng(geo.lat, geo.lon);
                var map = new google.maps.Map($id('map'), {
                    center: marker,
                    zoom: 17
                });
                new google.maps.Marker({
                    position: marker,
                    map: map,
                    title: geo.display_name
                });
            }
        }, function() {
            var map = $id('map');
            map.setAttribute('class', 'error');
            var message = 'У вашому браузері неможливо відобразити карту<br />';
            if (IE && IEVersion < 10) {
                message += 'Для відображення потрібен Internet Explorer 10 або новіший';
            }
            map.innerHTML = message;
        });
    });
});
