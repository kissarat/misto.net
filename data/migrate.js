const
    mysql = require('mysql'),
    postgres = require('pg'),
    Iconv = require('iconv').Iconv,
    config = require('../config');

const my = mysql.createConnection({
    user: 'root',
    password: '1',
    database: 'misto'
});
my.connect();

const tables = ['firm', 'cat', 'good', 'firm_cat', 'firm_good'];

const pg = new postgres.Client(process.env.DATABASE_URL || config.db);
const iconv = new Iconv('windows-1251', 'utf8');

function migrate(in_table, out_table, in_columns, out_columns, convert) {
    return function() {
        var placeholders = [];
        for(var i=1; i<=in_columns.length; i++)
            placeholders.push('$' + i);
        if (!out_table)
            out_table = in_table;
        if (!out_columns)
            out_columns = in_columns;
        var insert = 'insert into ' + out_table + '(' + out_columns.join(',') + ') values (' + placeholders.join(',') + ')';
        out_columns = out_columns.join(',');
        convert = convert || function(_) {
            var result = [];
            for(var i in in_columns)
                result.push(_[in_columns[i]]);
            return result;
        };
        my.query('select ' + in_columns.join(',') + ' from ' + in_table,
            wrap(function(rows, fields) {
                console.log(in_table + ' ' + rows.length);
                var check = exit_check(rows.length);
                for(var i=0; i<rows.length; i++) {
                    pg.query(insert, convert(rows[i]), check);
                }
            }));
    }
}

var migration = {
    firm: migrate('firms', 'firm',
        ['firm_id', 'mark', 'name', 'address', 'phone', 'email', 'homepage', 'work_hours', 'advertise', 'search_str'],
        ["id",      'rank', "name", "address", "phone", "email", "site",     "working",    "ad",        'search'],
        function(_) {
            var ad = _.advertise;
            ad = ad.length > 20 ? iconv.convert(ad).toString() : null;
            return [_.firm_id, _.mark, _.name, $(_.address), $(_.phone), $(_.email),
                $(_.homepage), $(_.work_hours), ad, $(_.search)];
        }
    ),

    cat: migrate('rubricator', 'cat',
        ['rubricator_bit', 'parent', 'level', 'name', 'search_str'],
        ['id',             'parent', 'level', 'name', 'search']
    ),

    good: migrate('goods', 'good',
        ['goods_id', 'rubricator_bit', 'name', 'search_str'],
        ['id',       'cat',            'name', 'search']
    ),

    firm_good: migrate('firm_good', null, ['firm', 'good']),
    firm_cat: migrate('firm_cat', null, ['firm', 'cat'])
};

function migrate_loop() {
    if (0 == tables.length)
        process.exit(0);
    var table = tables.shift();
    j = 0;
    migration[table]();
}

var j = 0;
function exit_check(count) {
    return wrap(function() {
        j++;
        process.stdout.write(j + '\r');
        if (j == count)
            migrate_loop();
    });
}

pg.connect(wrap(function() {
    migrate_loop();
}));

function $(str) {
    return null != str && str.length == 0 ? null : str;
}

function error_dump(obj) {
    if (!obj)
        return;
    console.error(JSON.stringify(obj));
}

function wrap(call) {
    return function(err, data) {
        if (err) {
            console.error(err);
            return;
        }
        call(data);
    }
}
