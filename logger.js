var intel = require('intel');
var fs = require('fs');

if (fs.existsSync('log'))
    intel.basicConfig({
        file: 'log/debug'
    });

/*
intel.config({
    formatters: {
        simple: {
            format: '[%(levelname)s] %(message)s',
            colorize: true
        },
        details: {
            format: '[%(date)s] %(levelname)s: %(message)s',
            strip: true
        }
    },

    handlers: {
        terminal: {
            class: intel.handlers.Console,
            formatter: 'simple',
            level: intel.TRACE
        },
        file: {
            class: intel.handlers.File,
            file: 'log/debug',
            formatter: 'details',
            level: intel.TRACE
        }
    },

    loggers: {
        terminal: {
            handlers: ['terminal'],
            level: 'TRACE',
            handleExceptions: true,
            exitOnError: false,
            propagate: false
        },
        file: {
            handlers: ['file'],
            level: 'TRACE'
        }
    }
});
*/

module.exports = intel;
global.log = intel.getLogger();
