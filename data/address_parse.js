const
    postgres = require('pg'),
    config = require('../config');

const pg = new postgres.Client(process.env.DATABASE_URL || config.db);

var j = 0;
function check(count) {
    return wrap(function() {
        process.stdout.write(++j + '\r');
        if (count == j)
            process.exit();
    });
}


function trim(str, apx) {
    if (str) {
        if (apx)
            str = str.replace(apx, '');
        return str.trim();
    }
    return null;
}

function parse_number(number) {
    number = trim(number);
    if (number) {
        var num_abc = /(\d+) *([абв])/i.exec(number);
        if (num_abc)
            number = num_abc[1] + num_abc[2].toLowerCase();
        else {
            number = /(\d+)/.exec(number);
            if (number)
                number = number[1];
        }
        return number;
    }
    return null;
}

const rex_city = /(м\.|смт\.|с\.)/;
const rex_street = /(вул\.|бульв\.|просп\.|про|пров\.|майдан)/;

pg.connect(wrap(function() {
    pg.query("select id, name from firm where name like '%№%'", wrap(function(result) {
        for(var i in result.rows) {
            var _ = result.rows[i];
            var number = /№ *(\d+)/.exec(_.name);
            if (number) {
                pg.query('update firm set "number"=$2 where id=$1', [_.id, number[1]]);
            }
        }
    }));

    pg.query('select id, address from firm', wrap(function(result) {
        for(var i in result.rows) {
            var _ = result.rows[i];
            var address = _.address.split(';');
            var city = address[1].replace('м.', '');
            var area = address[1].replace('район', '');
            var street;
            var house;
            if (city.length != address[1].length) {
                city = trim(city);
                address = address[3] ? address[3].split(',') : address.slice(2);
                street = trim(address[0], rex_street);
                house = parse_number(address[1]);
                pg.query('update firm set city=$2, street=$3, house=$4 where id=$1',
                    [_.id, city, street, house], check(result.rows.length));
            } else if (area.length != address[1].length) {
                area = trim(area);
                address = address[2].split(',');
                city = trim(address[0], rex_city);
                street = trim(address[1], rex_street);
                house = parse_number(address[2]);
                pg.query('update firm set area=$2, city=$3, street=$4, house=$5 where id=$1',
                    [_.id, area, city, street, house],  check(result.rows.length));
            }
        }
    }));
}));

function wrap(call) {
    return function(err, data) {
        if (err) {
            console.error(err);
            return;
        }
        call(data);
    }
}
