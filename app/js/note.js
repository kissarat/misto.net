"use strict";

app.controller('NoteController', function NoteController($scope, res, params) {
    $scope.id = parseInt(params.id) || 0;
    var note = new res('/note', {id:$scope.id});
    $scope.notes = note.query();
    $scope.is_topic = false;
    $scope.submit = function() {
        if ($scope.form.$valid) {
            var data = {
                text:$scope.text,
                parent: $scope.id
            };
            if ($scope.is_topic)
                data.topic = $scope.topic;
            data.parent = $scope.id;
            $scope.notes = note.save(data);
        }
    };
    $('input[name="font-size"]').change(function() {
        var width = parseFloat(this.value);
        get_css_rule('.note').style.width = width > 0 ? (width + 'cm') : '100%';
    });
});
