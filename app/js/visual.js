"use strict";
/**
 * @file animation + icons
 */

var namespace = {
    svg: 'http://www.w3.org/2000/svg',
    xlink: 'http://www.w3.org/1999/xlink'
};

if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function(call) {
        setTimeout(call, 40);
    };

$(window)
    .load(function() {
        var iconbag = $('[rel="icon"]').attr('href');
        $('[data-icon]')
            .attr('xmlns', namespace.svg)
            .attr('width', '24')
            .attr('height', '24')
            .each(function(i, icon) {
                var use = document.createElementNS(namespace.svg, 'use');
                use.setAttributeNS(namespace.xlink, 'href', iconbag + '#' + icon.dataset.icon);
                icon.appendChild(use);
                //icon.innerHTML = '<use xlink:href="' + iconbag + '#' + icon.dataset.icon + '"/>';
            });

        phone_first_digit = phone.querySelector('div:first-child');
        if (features.bind)
            animate_phone.call(phone_first_digit);

        collapse.onclick = animate_collapse;

        if (window.localStorage && 'none' == localStorage['header.style.display'])
            animate_collapse();
    })
    .unload(function() {
        if (window.localStorage)
            localStorage['header.style.display'] = $('header').css('display');
    });

var phone_first_digit;
var animate_phone_last = Date.now();
var animate_phone_back = false;

function animate_phone() {
    var top = parseFloat(this.style.marginTop) || 0;
    top = Math.sqrt(top);
    var now = Date.now();
    var step = (now - animate_phone_last)/25;
    if (step > 1)
        step = 1;
    top += animate_phone_back ? -step : step;
    if (top > 3)
        animate_phone_back = true;
    animate_phone_last = now;
    var next = animate_phone.bind(this);
    var delay = 0;
    if (top > 0) {
        this.style.marginTop = top * top + 'px';
    }
    else {
        animate_phone_back = false;
        this.style.removeProperty('margin-top');
        next = this.nextElementSibling;
        if (!next)
            next = phone_first_digit;
        else if ('-' == next.innerHTML)
            next = next.nextElementSibling;
        delay = next == phone_first_digit ? 5000 : 120;
        next = animate_phone.bind(next);
    }
    if (delay > 0)
        setTimeout(next, delay);
    else
        if (!(next == phone_first_digit && 'none' == $('header').css('display')))
            requestAnimationFrame(next);
}

function animate_collapse() {
    var $collapse = $('#collapse');
    $collapse.find('> div').css('display', 'none');
    $('header').toggle('drop', {direction:'up'}, function () {
        if ('none' == $('header').css('display')) {
            $collapse.find('> div:first-child').css('display', 'none');
            $collapse.find('> div:last-child').css('display', '');
        }
        else {
            $collapse.find('> div:first-child').css('display', '');
            $collapse.find('> div:last-child').css('display', 'none');
            if (features.bind)
                animate_phone.call(phone_first_digit);
        }
    });
}


function get_css_rule(selector) {
    var stylesheet = matchFirst(document.styleSheets, 'href', /app.css$/);
    return findFirst(stylesheet.cssRules, 'selectorText', selector);
}

var val;

function error_dump(obj) {
    val = obj;
    console.error(obj);
}
