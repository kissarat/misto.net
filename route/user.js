"use strict";

function route_user(req, res) {
    switch(req.method) {
        case 'GET':
            db.query('select "username", "email" from "user"', send_rows(res));
            break;
        case 'DELETE':
            db.query('delete from "user" where "username"=$1', [req.query.username], wrap(res, function() {
                res.end();
            }));
            res.end();
            break;
        default:
            send_status(res, 405);
            break;
    }
}

function admin(req, res) {
    auth(req, res, function(user) {
        if ('admin' == user.username)
            route_user(req, res);
    });
}

module.exports = admin;
