"use strict";
global.config = require('./config.json');
require('./logger');

var body = require('body-parser');
var crypto = require('crypto');
var express = require('express');
var fs = require('fs');
var postgres = require('pg');
var sitemap = require('./route/sitemap');

try {
    var render = require('./render');
}
catch(ex) {
    log.error('No render');
}


Object.freeze(config);
global.db = new postgres.Client(process.env.DATABASE_URL || config.db);
global.app = express();
app.use(body.urlencoded({
    type: 'application/x-www-form-urlencoded',
    extended: false
}));
app.use(body.json({
    type: 'application/json'
}));

//if (!process.env.NGINX)
    var express_static = express.static(__dirname + '/app');

app.use(function(req, res, next) {
    var dev = config.dev || process.env.DEV;
    if (dev && dev != req.connection.remoteAddress)
        //if (process.env.NGINX)
        //    return req.redirect('/dev.html');
        //else
            req.url = '/dev.html';
    //if (process.env.NGINX)
    //    next();
    //else
    if ('/sitemap.xml' == req.url)
        return sitemap(req, res);
    var user_agent = req.headers['user-agent'] || '';
    var accept = req.headers.accept || '';
    var is_html = accept.indexOf('text/html') >= 0;
    var dot = req.url.indexOf('.');
    dot = dot < 0 || dot > req.url.indexOf('?');
    dot = dot && is_html && 'GET' == req.method;
    if (render && dot && /Googlebot|YandexBot|bingbot|Yahoo!|baidu\.com/.test(user_agent))
        render(req, res);
    else {
        if (dot)
            req.url = '/index.html';
        express_static(req, res, next);
    }
});

require('./route/auth');
app.post('/signup', signup);
app.post('/login', login);
app.get('/logout', logout);
var firm = require('./route/firm');
app.use('/firm', firm.firm);
app.use('/search', firm.search);
var cat = require('./route/cat');
app.use('/cat/search', cat.search);
app.use('/cat', cat.cat);
app.use('/note/:id?', require('./route/note'));
app.use('/user', require('./route/user'));

db.connect(function(err) {
    if (err) {
        log.critical(err);
        process.exit(-1);
    }
    //if (process.env.NGINX) {
    //    setTimeout(function () {
    //        fs.chmodSync(config.http.socket, '777');
    //    }, 4000);
    //    app.listen(config.http.socket);
    //}
    //else
        app.listen(
            process.env.PORT || config.http.port || 8080,
            process.env.IP ||process.env.HOSTNAME || config.http.host || 'localhost'
        );
});

function wrap(res, call) {
    if (!call)
        call = res;
    return function(err, data) {
        if (err) {
            log.error(err);
            if (call != res)
                res.end(JSON.stringify({
                    error: err
                }));
            return;
        }
        call(data);
    }
}

function query(q, params, call) {
    q = q.join(' ');
    var i = 0;
    log.debug(q.replace(/\$/g, function() {
       return params[i++];
    }));
    i = 1;
    q = q.replace(/\$/g, function() {
        return '$' + i++;
    });
    db.query(q, params, call);
}

function object2sql(values, object) {
    var result = [];
    for(var name in object) {
        values.push(object[name]);
        result.push('"' + name + '"=$' + values.length);
    }
    return result;
}

function quote4sql(array) {
    var result = [];
    for(var i = 0; i < array.length; i++)
        result.push('string' == typeof array[i] ? '"' + array[i] + '"' : array[i]);
    return result;
}

function update(res, table, conditions, properties) {
    var values = [];
    conditions = object2sql(values, conditions).join(' and ');
    properties = object2sql(values, properties).join(', ');
    var sql = 'update "' + table + '" set ' + properties + ' where ' + conditions;
    log.debug(sql + '\n' + quote4sql(values).join(', '));
    db.query(sql, values,
        wrap(res, function() {
            res.sendStatus(200);
        }));
}

function send_rows(res) {
    return wrap(res, function(result) {
        res.json(result.rows);
    });
}

function rand(min, max) {
    if ('number' != typeof max) {
        max = min;
        min = 0;
    }
    return min + Math.floor((max - min)*Math.random());
}

function hash(str) {
    var hash = crypto.createHash(config.password.hash);
    hash.update(str);
    return hash.digest('bin');
}

function dump_args() {
    console.log(arguments);
}

function report(res, call) {
    return function(err, data) {
        if (err) {
            res.json({
                error: err
            });
            log.error(err);
            return;
        }
        call(data);
    }
}

function receive(req, call) {
    var data = [];
    req.setEncoding('utf8');
    req.on('data', function(chunk) {
       data.push(chunk);
    });
    req.on('end', function() {
        data = data.join('');
        data = JSON.parse(data);
        call(data);
    });
}

global.wrap = wrap;
global.query = query;
global.send_rows = send_rows;
global.rand = rand;
global.hash = hash;
global.dump_args = dump_args;
global.report = report;
global.receive = receive;
global.update = update;
