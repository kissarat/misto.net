"use strict";

app.controller('MenuController', function MenuController($scope, $cookies) {
    $scope.menu = [
        {name:'Пошук',      url:'search'},
        {name:'Рубрикатор', url:'cat'}
    ]
        .concat($cookies.get('sid')
            ? [
            {name:'Написати',   url:'note'},
            {name:'Користувачі',url:'user'},
            {name:'Вихід',      url:'logout'}
        ]
            : [{name:'Вхід',     url:'login'}, {name:'Реєстрація', url:'signup'}]
    );
});
