"use strict";

app.controller('UserController', function UserController($scope, res) {
    var user = new res('/user');
    $scope.users = user.query();

    $scope.remove = function(username) {
        user.remove({username:username}, function() {
            $('[data-username="' + username + '"]').remove();
        })
    }
});
