#!/usr/bin/env bash
mysql -utaras -p1 misto < data.sql &&
mysql -utaras -p1 misto < mysql.ext.sql &&
psql misto -f schema.drop.sql &&
psql misto -f schema.sql &&
node migrate.js &&
node address_parse.js
