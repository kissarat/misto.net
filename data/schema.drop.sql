-- drop database "misto"
drop table if exists "firm";
drop table if exists "good";
drop table if exists "cat";
drop table if exists "firm_cat";
drop table if exists "firm_good";
drop view if exists "sorted_note";
drop table if exists "note";
drop table if exists "user";
drop domain if exists "username";
drop domain if exists "insert_timestamp";
drop trigger if exists "tags_update" on "searchable";
drop table if exists "searchable";
drop function if exists "tags_update"();
drop text search configuration "ukr";
drop text search dictionary "ukr";