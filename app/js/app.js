"use strict";
var app = angular.module('mistoApp', ['ngRoute', 'ngResource', 'ngCookies'])
    .config(function($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider.when('/', {
            redirectTo:'/search'
        });
        $routeProvider.when('/search', {
            templateUrl: 'views/search.html',
            controller: 'SearchController'
        });
        $routeProvider.when('/firm/edit', {
            templateUrl: 'edit/firm.html',
            controller: 'FirmController'
        });
        $routeProvider.when('/firm', {
            templateUrl: 'views/firm.html',
            controller: 'FirmController'
        });
        $routeProvider.when('/cat', {
            templateUrl: 'views/cat.html',
            controller: 'CatController'
        });
        $routeProvider.when('/signup', {
            templateUrl: 'views/signup.html',
            controller: 'SignupController'
        });
        $routeProvider.when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        });
        $routeProvider.when('/logout', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        });
        $routeProvider.when('/user', {
            templateUrl: 'views/user.html',
            controller: 'UserController'
        });
        $routeProvider.when('/note', {
            templateUrl: 'views/note.html',
            controller: 'NoteController'
        });
        $routeProvider.otherwise({
           templateUrl: 'views/404.html'
        });
    });
