"use strict";

var phantom = require('phantom');

module.exports = function(req, res) {
    phantom.create(function(ph) {
        ph.createPage(function (page) {
            page.open('http://localhost:8080' + req.url, function (status) {
                page.evaluate(function () {
                    var base = document.querySelector('base');
                    if (base)
                        base.setAttribute('href', 'http://localhost:8080');
                    var serializer = new XMLSerializer();
                    return serializer.serializeToString(document);
                }, function(result) {
                    res.end(result);
                })
            });
        });
    });
};
