"use strict";

app.controller('CatController', function CatController($scope, $location, params, res) {
    var cat = new res('/cat');
    $scope.cats = cat.query(params, function(data) {
        if (1 == data.length) {
            $location.path('/search').search({cat: params.id});
        }
        if (params.id)
            $scope.parent = data.pop();
    });
    $scope.go = function(id) {
        params.id = id;
        $location.search(params);
    }
});