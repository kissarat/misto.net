var cache;

module.exports = function(req, res) {
    res.setHeader('Content-Type', 'text/xml; charset=utf-8');
    res.setHeader('Last-Modified', 'Sat, 25 Apr 2015 09:11:14 +0000');
    if (cache) {
        res.end(cache);
        return;
    }
    db.query('select id from firm',
        wrap(function(result) {
            var location = '<url><loc>http://' + (req.domain || 'localhost:8080') + '/firm?id=';
            var xml = [
                '<?xml version="1.0" encoding="UTF-8"?>\n',
                '<?xml-stylesheet type="text/xsl" href="sitemap.xsl"?>\n',
                '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
            ];
            result = result.rows;
            for (var i = 0; i < result.length; i++) {
                xml.push(location);
                xml.push(result[i].id);
                xml.push('</loc></url>');
            }
            xml.push('</urlset>');
            xml = xml.join('');
            sitemap = xml;
            res.end(xml);
        }));
};
