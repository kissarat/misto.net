"use strict";

app.factory('res', function($q, $http, $location) {
    function res(url, defaults) {
        this.url = url;
        this.defaults = defaults;
    }

    res.prototype = {
        get: function(values, success, error) {
            return this.http({
                method: 'GET',
                params: values
            }, success, error);
        },

        query: function(values, success, error) {
            return this.http({
                method: 'GET',
                params: values,
                isArray: true
            }, success, error);
        },

        save: function(values, success, error) {
            values = poorify(values);
            for(var name in values)
                if ('$' == name[0])
                    delete values[name];
            return this.http({
                method: 'POST',
                data: values
            }, success, error);
        },

        remove: function(values, success, error) {
            return this.http({
                method: 'DELETE',
                params: values
            }, success, error);
        },

        http: function(config, success, error) {
            config.params = config.params || {};
            set_defaults(config.params, this.defaults);
            config.url = this.url;
//            var result = store.get(config.url, config.params);
//            if (result) {
//                if (success)
//                    success(result);
//                return result;
//            }
//            else
                var result = config.isArray ? [] : {};
            delete config.isArray;
            var deffered = $q.defer();
            result.$promise = deffered.promise;
            $http(config)
                .success(function(data) {
                    if (success)
                        success(data);
                    for(var key in data)
                        result[key] = data[key];
//                    store.put(config.url, config.params, data);
                    deffered.resolve(data);
                })
                .error(function(data, status) {
                    if (403 == status) {
                        $location.path('/login');
                        $location.replace();
                    }
                    deffered.reject(status);
                });
            if (error)
                result.$promise.catch(error);
            return result;
        }
    };

    return res;
})
.factory('params', function($routeParams) {
        for(var key in $routeParams) {
            var param = $routeParams[key];
            param = parseInt(param);
            if (!isNaN(param))
                $routeParams[key] = param;
        }
        return $routeParams;
    });

app.controller('ErrorController', function ErrorController() {

});

function set_defaults(target, defaults) {
    for(var key in defaults)
        if (!(key in target))
            target[key] = defaults[key];
    return target;
}

function poorify(object) {
    for(var name in object)
        if ('object' == typeof object[name])
            delete object[name];
    return object;
}

function exclude(obj, exclude_key) {
    var result = {}; //Object.create(null);
    for(var key in obj)
        if (exclude_key != key)
            result[key] = obj[key];
    return result;
}

// Cache
var store = {
    get: function (url, params) {
        var cache = this[url];
        for(var i in cache) {
            var entry = cache[i][0];
            var match = true;
            for(var key in entry)
                if (params[key] != entry[key]) {
                    match = false;
                    break;
                }
            if (match)
                return cache[i][1];
        }
        return null;
    },

    put: function(url, params, obj) {
        if (!(this[url] instanceof Array))
            this[url] = [];
        this[url].push([params, obj]);
    }
};
