-- create database "misto" owner "taras" encoding "UFT8"
-- connection limit 5

create text search configuration ukr (
  parser = default);

create text search dictionary ukr (
  template = ispell,
  DictFile = 'uk_ua',
  AffFile = 'uk_ua'
);

ALTER TEXT SEARCH CONFIGURATION ukr ADD MAPPING
FOR email, url, url_path, host, file, version,
sfloat, float, int, uint,
numword, hword_numpart, numhword
WITH simple;

ALTER TEXT SEARCH CONFIGURATION ukr ADD MAPPING
FOR word, hword_part, hword
WITH ukr;

-- set default_text_search_config = ukr;
alter database misto set default_text_search_config to ukr;

create table "searchable"(
  "search" text,
  "tags" tsvector
);

create or replace function "tags_update"()
  returns trigger as $$
  begin
    new.tags = to_tsvector(new.search);
    return new;
  end;
  $$ language 'plpgsql';

create trigger "tags_update" before insert or
update of "search" on "searchable"
for each row execute procedure "tags_update"();

create table "firm" (
  "id" serial primary key,
  "name" varchar(128),
  "address" varchar(255),
  "phone" varchar(128),
  "email" varchar(128),
  "site" varchar(128),
  "working" varchar(128),
  "ad" text,
  "rank" int not null,
  "number" smallint,
  "city" varchar(48),
  "area" varchar(16),
  "street" varchar(64),
  "house" varchar(4),
  "lat" double precision check(lat >=-90 and lat  <= 90),
  "log" double precision check(log > -180 and log <= 180)
) inherits("searchable");

CREATE table "cat" (
  "id" bigserial primary key,
  "parent" bigint not null,
  "level" smallint,
  "name" varchar(64)
) inherits("searchable");

create table "firm_cat" (
  "firm" int not null,
  "cat" bigint not null,
  constraint "firm_cat_unique" unique ("firm", "cat")
--   constraint "firm_cat_firm" foreign key("firm")
--     references "firm"("id"),
--   constraint "firm_cat_cat" foreign key("cat")
--     references "cat"("id")
);

create table "good" (
  "id" serial primary key,
  "cat" bigint references "cat"("id"),
  "name" varchar(255)
) inherits("searchable");

create table "firm_good" (
  "firm" int not null,
  "good" int not null,
  constraint "firm_good_unique" unique ("firm", "good")
--   constraint "firm_good_firm" foreign key("firm")
--     references "firm"("id"),
--   constraint "firm_good_good" foreign key("good")
--     references "good"("id")
);

create domain "username" as varchar(16) check (length(value) > 4);
create domain "insert_timestamp" timestamp without time zone default localtimestamp;

create table "user" (
"username" "username" primary key not null,
"email" varchar(64) not null unique check (email ~* '^[a-z0-9._%-]+@[a-z0-9.-]+[.][a-z]+$'),
"verified" "insert_timestamp",
"password" bytea not null,
"sid" char(24) not null unique
);

create table "note" (
  "id" serial primary key,
  "author" "username" not null references "user"("username")
    on delete cascade,
  "text" text not null,
  "ip" inet,
  "parent" int references "note"("id"),
  "created" "insert_timestamp",
  -- topic
  "topic" varchar(128),
  "order" smallint not null default 0
);

create view "sorted_note" as
  select "id", "author", "created", "text", "parent", "topic" from "note"
  order by (case when "topic" is null then 0 else 1 end) desc,
      "order" desc, "created";
/*
create table "firm_tag" (
  "firm" int not null references "firm"("id"),
  "tag" char(8),
  "author" "username" not null references "user"("username"),
  constraint "firm_tag_unique" unique ("firm", "tag")
);
*/
insert into "user"("username", "email", "password", "sid")
	values ('admin', 'kissarat@gmail.com',
	E'\\xd033e22ae348aeb5660fc2140aec35850c4da997', 'd033e22ae348aeb5660fc214');

insert into "note"("id", "author", "text", "topic")
	values (0, 'admin', 'Опис', 'Корінь');
