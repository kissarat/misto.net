"use strict";
var Cookies = require('cookies');
var createTransport = require('nodemailer').createTransport;
var fs = require('fs');
var jade = require('jade');

var session_age = config.session.age * 60 * 1000;
var session_clear_interval = config.session.clear_interval * 60 * 1000;

var session = Object.create(null);
function get_session(sid) {
    var data = session[sid];
    if (!data || session_expired(sid))
        return null;
    data.last = Date.now();
    return data;
}

function set_session(sid, data) {
    delete data.sid;
    data.last = Date.now();
    session[sid] = data;
}

function session_expired(sid) {
    if ((Date.now() - session[sid].last) > session_age)  {
        delete session[sid];
        return true;
    }
    return false;
}

function clear_session() {
    for(var sid in session)
        session_expired(sid);
}

function set_user(sid, data, res) {
    set_session(sid, data);
    res.cookie('sid', sid, {maxAge:session_age});
    res.cookie('username', data.username, {maxAge:session_age});
}

function gen_password(length, chars) {
    var _ = config.password;
    length = length || rand(_.min, _.max);
    chars = chars || _.chars;
    var password = [];
    for(var i=0; i<length; i++)
        password.push(chars[rand(chars.length)]);
    return password.join('');
}

function gen_sid() {
    return gen_password(config.session.length, config.session.chars);
}

var signup_email = jade.compile(fs.readFileSync('template/signup.jade'));

function send_validation_mail(user, call) {
    var transport = createTransport("Gmail", config.smtp);
    transport.sendMail({
        from: 'kissarat@gmail.com',
        to: user.email,
        subject: 'Реєстрація',
        html: signup_email(user)
    }, call);
}

global.auth = function(req, res, call) {
    var cookies = new Cookies(req, res);
    var sid = cookies.get('sid');
    if (!sid) {
        res.writeHead(403);
        res.end();
        return;
    }
    var data = get_session(sid);
    if (data) {
        call(data);
        return;
    }
    db.query('select "username", "email" from "user" where "sid"=$1', [sid], wrap(function(result) {
        if (1 != result.rows.length) {
            res.redirect('/login');
            return;
        }
        data = result.rows[0];
        set_user(sid, data, res);
        call(data);
    }));
};

global.signup = function(req, res) {
    var user = {
        username: req.body.username,
        email: req.body.email,
        password: gen_password()
    };
    send_validation_mail(user, wrap(function(result) {
        db.query('insert into "user" ("username", "email", "password", "sid") values ($1,$2,$3,$4)',
            [user.username, user.email, hash(user.password), gen_sid()], wrap(function() {
                res.redirect('/');
            }));
    }));
};

global.login = function(req, res) {
    db.query('select "username", "email", "verified", "sid" from "user" where "username"=$1 and "password"=$2',
        [req.body.username, hash(req.body.password)], wrap(function(result) {
            if (1 == result.rows.length) {
                var user = result.rows[0];
                set_user(user.sid, user, res);
                res.redirect('/');
                if (null != user.verified) {
                    db.query('update "user" set "verified"=null where "username"=$1', [user.username]);
                }
            }
            else
                res.redirect('/login');
        }));
};

global.logout = function(req, res) {
    var cookies = new Cookies(req, res);
    var sid = cookies.get('sid');
    delete session[sid];
    res.clearCookie('sid');
    res.redirect('/');
};

setInterval(clear_session, session_clear_interval);
